### Additional Notes on the CI server.

#### Modification for PopHR Jenkinsfile.
See Maxime Lavigne if you have question about the content of this section. On April 18, 2019 a series of modifcations were applied to fix broken build system resulting from an updated of the JenkinsCI.

The modifications were made because "node" wasn't recognized anymore, and "docker" didn't have the right permissions.

**connecting to the JenkinsVM**
>docker exec -it --user root jenkins /bin/bash

**Node**
Jenkins now uses a different way of scoping certain tools, and this made our tools no longuer recognized globally. So instead, I connected to the Builder VM and installed node manually. (you will have to connect using the root)

>apk add --update --no-cache nodejs nodejs-npm

**Docker**
This was a hard issue, since it's permission related and related to having the correct User+Group and for these to have the right userID and groupID. So, after spending a good deal of time trying to get this working (which wasn't trivial and even when the exact same uid and gid were used, strange and difficult to track bug ensued).

Hence, I will just make the docker executable usable by any user. So, on the host machine :

>sudo chmod 666 /var/run/docker.sock

