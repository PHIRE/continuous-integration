# VM Builder - Continuous Integration and DevOps

This repo contains the information needed to create and maintain VM-Builder.

VM-Builder uses docker and an nginx reverse proxy of the following services:
* Archiva (/archiva)
* Jenkins (/jenkins)
* Docker Registry

Hence, the following port are open:
* 22 (ssh) : _For terminal connection_
* 80 (http) : _For the reverse proxy_
* Any other endpoint in the server should return 404 not found

For any question, contact the maintainer :
* Maxime Lavigne (malavv) maxime.lavigne@mail.mcgill.ca

## Initial Configuration

VM-Builder uses a GNU/Linux Ubuntu LTS distribution. First, update the os, then
[configure unattended updates](https://help.ubuntu.com/lts/serverguide/automatic-updates.html).
Then [install Docker CE](https://docs.docker.com/install/linux/docker-ce/ubuntu/) and
[install docker-compose](https://github.com/docker/compose/releases).


## Update a service

You will need to know 2 things to update a service. First, the reference image:
* nginx is ``nginx:mainline``
* jenkins is ``jenkinsci/blueocean``

Then the container name:
* nginx is ``nginx``
* jenkins is ``jenkins``

~~~
docker pull {reference image}
docker stop {container name}
docker rm {container name}
docker-compose -f builder-ci/docker-compose.yml up -d
~~~

## Perform the initial installation

### Install Requirements

~~~
sudo -i
apt install apt-transport-https ca-certificates curl git software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt update
apt install docker-ce
curl -L https://github.com/docker/compose/releases/download/1.15.0/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
exit
~~~

### Clone the configuration
``git clone --depth=1 http://git.mchi.mcgill.ca/PHIRE/continuous-integration.git builder-ci``

### Docker Volumes

**Creation of:** `docker volume create $VOLUME_NAME`

**Listing of:** `docker volume ls`

**Backup and Restore**
Both are handled using another docker image to ensure that we can backup the named volume without needing to get the container up and running first.

[GitHub Project for Mgmt of Docker Volume](https://github.com/loomchild/volume-backup)

Example
```{bash}
# Backup archiva
docker run -v archiva_data:/volume -v /backup:/backup --rm loomchild/volume-backup backup archiva_$(date +"%Y-%m-%d")
# Restore Archiva
docker run -v archiva_data:/volume -v $(pwd):/backup --rm loomchild/volume-backup restore archiva_$(date +"%Y-%m-%d")
```
